<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Person;
use App\Entity\Structure;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContactController extends AbstractController
{

    private $serializer;
    private $em;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var GeocodeAPI
     */
    private $geocodeAPI;

    public function __construct( EntityManagerInterface $em, ValidatorInterface $validator, GeocodeAPI $geocodeAPI)
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $this->serializer = new Serializer($normalizers, $encoders);
        $this->em = $em;
        $this->validator = $validator;
        $this->geocodeAPI = $geocodeAPI;
    }

    /**
     * @Route("/contact/add", name="contact_add", methods={"POST"})
     */
    public function postContact(Request $request): JsonResponse
    {
        /** @var Contact $contact */
        $contact = $this->serializer->deserialize($request->getContent(), Contact::class, 'json');
        /** @var Person $person */
        $person = $this->serializer->deserialize($request->getContent(), Person::class, 'json');
        /** @var Structure $structure */
        $structure = $this->serializer->deserialize($request->getContent(), Structure::class, 'json');
        if($structure instanceof Structure){
            $contact->setStructure($structure);
        }else if($person instanceof Person){
            $contact->setPerson($person);
        }

        $violations = $this->validator->validate($contact);
        $errors =[];
           foreach ($violations as $violation){
               $errors[] = $violation;
           }

        if(count($violations) > 0){
         return new JsonResponse([ 'status' => 422, 'errors'=> json_encode($errors)],422);
        }

        //@TODO get geocode address + can use dispatch message async for call API
        if(!empty($contact->getPostalAddress())){
           $response = $this->geocodeAPI->fetchGeoCodeInformation($contact->getPostalAddress());
        }

        $this->em->persist($contact);
        $this->em->flush();

        return new JsonResponse([ 'status' => 200, 'contact'=> json_decode($request->getContent())],JsonResponse::HTTP_CREATED );

    }
}
