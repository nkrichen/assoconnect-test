<?php

use Symfony\Contracts\HttpClient\HttpClientInterface;
class GeocodeAPI
{
    private $client;
    const YOUR_MAPBOX_ACCESS_TOKEN = "YOUR_MAPBOX_ACCESS_TOKEN";

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function fetchGeoCodeInformation($address): array
    {
        $response = $this->client->request(
            'GET',
            'https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=' .self::YOUR_MAPBOX_ACCESS_TOKEN
        );
        $statusCode = $response->getStatusCode();
        $contentType = $response->getHeaders()['content-type'][0];
        $content = $response->getContent();
        $content = $response->toArray();

        if($statusCode === 200){
          return $content;
        }else{
            return [];
        }
    }

}
